<!DOCTYPE html>
<html>
<head>
    <title>Contador de Múltiplos y Cantidad de Cifras</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 20px;
        }

        h1 {
            color: #333333;
        }

        form {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            width: 100%;
            padding: 10px;
            border: 1px solid #cccccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            background-color: #4caf50;
            color: #ffffff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }

        h2 {
            margin-top: 20px;
            font-size: 18px;
        }

        .result {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <h1>Contador de Múltiplos y Cantidad de Cifras</h1>

    <form method="post" action="">
        <label for="cifras">Cantidad de Cifras:</label>
        <input type="number" name="cifras" id="cifras" required><br><br>

        <label for="divisor">Divisor:</label>
        <input type="number" name="divisor" id="divisor" required><br><br>

        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $cifras = $_POST["cifras"];
        $divisor = $_POST["divisor"];

        $inicio = pow(10, $cifras - 1);
        $fin = pow(10, $cifras) - 1;

        $contador = 0;
        $multiplos = [];
        for ($i = $inicio; $i <= $fin; $i++) {
            if ($i % $divisor == 0) {
                $contador++;
                $multiplos[] = $i;
            }
        }

        echo "<div class='result'>";
        echo "<h2>Resultado:</h2>";
        echo "<p>Cantidad de múltiplos de $divisor con $cifras cifras: $contador</p>";
        echo "<p>Números múltiplos: " . implode(", ", $multiplos) . "</p>";
        echo "</div>";
    }
    ?>

</body>
</html>
