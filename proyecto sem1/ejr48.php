<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Permutaciones</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            margin: 0;
            padding: 20px;
        }

        h1 {
            color: #333333;
        }

        .container {
            background-color: #ffffff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            width: 100%;
            padding: 10px;
            border: 1px solid #cccccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            background-color: #4caf50;
            color: #ffffff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            border-radius: 3px;
        }

        .success {
            background-color: #4caf50;
            color: #ffffff;
        }

        .warning {
            background-color: #ff9800;
            color: #ffffff;
        }

        .error {
            background-color: #f44336;
            color: #ffffff;
        }
    </style>
</head>
<body>
    <h1>Calculadora de Permutaciones</h1>

    <div class="container">
        <h2>Inicio de Sesión</h2>

        <form method="post" action="">
            <label for="objetos">Cantidad de Objetos:</label>
            <input type="number" name="objetos" id="objetos" required><br><br>

            <input type="submit" value="Calcular">
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $objetos = $_POST["objetos"];

            if ($objetos > 0) {
                $resultado = 1;
                for ($i = 1; $i <= $objetos; $i++) {
                    $resultado *= $i;
                }

                echo "<div class='result success'>";
                echo "Cantidad de formas de ordenar $objetos objetos: $resultado";
                echo "</div>";
            } else {
                echo "<div class='result warning'>";
                echo "Ingrese una cantidad válida de objetos.";
                echo "</div>";
            }
        }
        ?>
    </div>

</body>
</html>
