<!DOCTYPE html>
<html>

<head>
    <title>Números capicúas</title>
    <style>
    table {
        border-collapse: collapse;
    }

    td {
        border: 1px solid black;
        width: 50px;
        height: 50px;
        text-align: center;
    }
    </style>
</head>

<body>
    <h1>Números capicúas de 3 cifras</h1>
    <table>
        <?php
      $count = 0;
      for ($i = 100; $i <= 999; $i++) {
        $reverse = strrev((string)$i);
        if ((string)$i === $reverse) {
          if ($count % 10 === 0) {
            echo "<tr>";
          }
          echo "<td>$i</td>";
          $count++;
          if ($count % 10 === 0) {
            echo "</tr>";
          }
        }
      }
    ?>
    </table>
</body>

</html>