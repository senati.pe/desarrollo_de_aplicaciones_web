<!DOCTYPE html>
<html>
<head>
	<title>Números primos de n cifras</title>
</head>
<body>
	<h1>Números primos de n cifras</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="cifras">Ingrese el número de cifras:</label>
		<input type="number" name="cifras" id="cifras" min="1" required>
		<br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
	function isPrime($num) {
		if ($num <= 1) {
			return false;
		}
		for ($i = 2; $i <= sqrt($num); $i++) {
			if ($num % $i == 0) {
				return false;
			}
		}
		return true;
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$cifras = $_POST["cifras"];
		$min = pow(10, $cifras - 1);
		$max = pow(10, $cifras) - 1;
		$count = 0;
		for ($i = $min; $i <= $max; $i++) {
			if (isPrime($i)) {
				$count++;
			}
		}
		echo "<p>Hay $count numeros primos de $cifras cifras.</p>";
	}
	?>
</body>
</html>