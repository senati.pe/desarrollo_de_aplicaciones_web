<!DOCTYPE html>
<html>
<head>
	<title>Suma y cantidad de la serie de Fibonacci</title>
</head>
<body>
	<?php
		if(isset($_POST['n']) && !empty($_POST['n'])){
			$n = (int) $_POST['n'];
			$prev = 0;$curr = 1;
			$sum = 0;$count = 0;
			while($curr < $n){
				if($curr < $n){
					$sum += $curr;
					$count++;
				}
				$temp = $prev;$prev = $curr;
				$curr = $temp + $prev;
			}
			echo "La suma de la serie de Fibonacci menor que ".$n." es: ".$sum."<br>";
			echo "La cantidad de números en la serie menor que ".$n." es: ".$count;
		}else{
	?>
	<form method="post">
		Introduce un número: <input type="number" name="n"><br>
		<input type="submit" value="Calcular">
	</form>
	<?php } ?>
</body>
</html>
