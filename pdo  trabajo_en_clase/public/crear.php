<?php
require "../conexion.php";
require "../common.php";


if (isset($_POST['submit'])) {
  if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

  try {
    $conexion = new PDO($dsn, $usuario, $contraseña);

    $new_user = array(
      "usuario" => $_POST['usuario'],
      "email"     => $_POST['email'],
      "contrasena" => $_POST['conrasena'],
      "apellido_paterno" => $_POST['apellido_paterno'],
      "apellido_materno"  => $_POST['apellido_materno'],
      "nombres"  => $_POST['nombres'],
      "edad"       => $_POST['edad'],
      "procedencia"  => $_POST['procedencia']
    );

    $sql = sprintf(
      "INSERT INTO %s (%s) values (%s)",
      "usuarios",
      implode(", ", array_keys($new_user)),
      ":" . implode(", :", array_keys($new_user))
    );

    $statement = $conexion->prepare($sql);
    $statement->execute($new_user);
  } catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
  }
}
?>
<?php require "templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) : ?>
  <blockquote><?php echo escape($_POST['apellido_paterno']); ?> Agregado exitosamente.</blockquote>
<?php endif; ?>

<h2>Agregar Usuario</h2>

<form method="post">
  <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
  <label for="usuarii">Usuario</label>
  <input type="text" name="usuario" id="usuario">
  <label for="contrasena">Contraseña</label>
  <input type="text" name="contrasena" id="contrasena">
  <label for="email">Email</label>
  <input type="text" name="email" id="email">
  <label for="apellido_paterni">Apellido Paterno</label>
  <input type="text" name="apellido_paterno" id="apellido_paterno">
  <label for="apellido_materno">Apellido Materno</label>
  <input type="text" name="apellido_materno" id="apellido_materno">
  <label for="nombres">Nombres</label>
  <input type="text" name="nombres" id="nombres">
  <label for="edad">Edad</label>
  <input type="text" name="edad" id="edad">
  <label for="procedencia">Procedencia</label>
  <input type="text" name="procedencia" id="procedencia">
  <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Regresar</a>

<?php require "templates/footer.php"; ?>
