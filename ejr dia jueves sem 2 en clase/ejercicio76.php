<!DOCTYPE html>
<html>
<head>
    <title>Cálculo del área de un rectángulo</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-image: url('76.jpg');
            background-size: cover;
            background-repeat: no-repeat;
        }

        h1 {
            text-align: center;
            color: #A569BD;
        }

        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
            border: 10px solid #ccc;
            border-radius: 5px;
            background-color: #000000;
            color: #ffffff;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            width: 100%;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            padding: 5px 10px;
            background-color: #9B59B6;
            border: none;
            color: #fff;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #AF7AC5;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            background-color: #5B2C6F;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Cálculo del área de un rectángulo</h1>

        <?php
        function calcularAreaRectangulo($base, $altura) {
            $area = $base * $altura;
            return $area;
        }

        if(isset($_POST['calcular'])){
            $base = $_POST['base'];
            $altura = $_POST['altura'];

            $area = calcularAreaRectangulo($base, $altura);
        ?>
        
        <div class="result">
            <h2>Resultado:</h2>
            <p>El área del rectángulo es: <?php echo $area; ?></p>
        </div>
        
        <?php
        } else {
        ?>
        
        <form method="POST" action="">
            <label for="base">Base:</label>
            <input type="number" name="base" id="base" required>
            <br><br>
            <label for="altura">Altura:</label>
            <input type="number" name="altura" id="altura" required>
            <br><br>
            <input type="submit" name="calcular" value="Calcular">
        </form>
        
        <?php
        }
        ?>
    </div>
</body>
</html>
