<!DOCTYPE html>
<html>
<head>
    <title>Determinar tipo de carácter</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-image: url('75.jpg');
            background-size: cover;
            background-repeat: no-repeat;
        }

        h1 {
            text-align: center;
            color: #D68910;
        }

        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
            border: 10px solid #ccc;
            border-radius: 5px;
            background-color: #000000;
            color: #ffffff;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="text"] {
            width: 100%;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            padding: 5px 10px;
            background-color: #AC5A2B;
            border: none;
            color: #fff;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #D26D33;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            background-color: #BA4A00;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Determinar tipo de carácter</h1>

        <?php
        if(isset($_POST['verificar'])){
            $caracter = $_POST['caracter'];

            $esVocal = false;
            $esMayuscula = false;
            $esMinuscula = false;
            $esNumero = false;
            $esSimbolo = false;

            if(preg_match('/[AEIOUaeiou]/', $caracter)){
                $esVocal = true;
            } elseif(ctype_upper($caracter)){
                $esMayuscula = true;
            } elseif(ctype_lower($caracter)){
                $esMinuscula = true;
            } elseif(ctype_digit($caracter)){
                $esNumero = true;
            } else {
                $esSimbolo = true;
            }
        ?>
        
        <div class="result">
            <h2>Resultado:</h2>
            <p>El carácter '<?php echo $caracter; ?>' es:</p>
            <ul>
                <?php if($esVocal): ?>
                <li>Una vocal</li>
                <?php endif; ?>
                <?php if($esMayuscula): ?>
                <li>Una letra mayúscula</li>
                <?php endif; ?>
                <?php if($esMinuscula): ?>
                <li>Una letra minúscula</li>
                <?php endif; ?>
                <?php if($esNumero): ?>
                <li>Un número</li>
                <?php endif; ?>
                <?php if($esSimbolo): ?>
                <li>Un símbolo</li>
                <?php endif; ?>
            </ul>
        </div>
        
        <?php
    } else {
        ?>
        
        <form method="POST" action="">
            <label for="caracter">Carácter:</label>
            <input type="text" name="caracter" id="caracter" required>
            <br><br>
            <input type="submit" name="verificar" value="Verificar">
        </form>
        
        <?php
        }
        ?>
    </div>
    </body>
    </html>