<!DOCTYPE html>
<html>
<head>
    <title>Area y perímetro de un cuadrado</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #898787; 
        }

        h1 {
            text-align: center;
            color: #ffffff; 
        }

        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
            border: 10px solid #ccc;
            border-radius: 5px;
            background-color: #000000; 
            color: #ffffff; 
        }
        label {
            display: block;
            margin-bottom: 10px;
        }
        input[type="text"] {
            width: 100%;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }
        input[type="submit"] {
            padding: 5px 10px;
            background-color: #D90303;
            border: none;
            color: #fff;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #030BFF;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            background-color: #9F9E9D;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Area y perímetro de un cuadrado</h1>

        <?php
        if(isset($_POST['calcular'])){
            $lado = $_POST['lado'];

            $area = $lado * $lado;
            $perimetro = 4 * $lado;
        ?>
        
        <div class="result">
            <h2>Resultados:</h2>
            <p>El área del cuadrado es: <?php echo $area; ?></p>
            <p>El perímetro del cuadrado es: <?php echo $perimetro; ?></p>
        </div>
        
        <?php
        } else {
        ?>
        
        <form method="POST" action="">
            <label for="lado">Lado del cuadrado:</label>
            <input type="text" name="lado" id="lado" required>
            <br><br>
            <input type="submit" name="calcular" value="Calcular">
        </form>
        
        <?php
        }
        ?>
    </div>
</body>
</html>
