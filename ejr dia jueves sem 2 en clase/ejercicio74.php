<!DOCTYPE html>
<html>
<head>
    <title>Suma de dígitos pares e impares</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-image: url('74.jpg');
            background-size: cover;
            background-repeat: no-repeat;
        }

        h1 {
            text-align: center;
            color: #6B7BB6;
        }

        .container {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
            border: 10px solid #ccc;
            border-radius: 5px;
            background-color: #000000;
            color: #ffffff;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            width: 100%;
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            padding: 5px 10px;
            background-color: #6B7BB6;
            border: none;
            color: #fff;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #C0C0C0;
        }

        .result {
            margin-top: 20px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 3px;
            background-color: #6B7BB6;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Suma de dígitos pares e impares</h1>

        <?php
        if(isset($_POST['calcular'])){
            $numero = $_POST['numero'];

            $sumaPares = 0;
            $sumaImpares = 0;

            $numeroString = (string)$numero;

            for($i = 0; $i < strlen($numeroString); $i++){
                $digito = intval($numeroString[$i]);

                if($digito % 2 == 0){
                    $sumaPares += $digito;
                } else {
                    $sumaImpares += $digito;
                }
            }
        ?>
        
        <div class="result">
            <h2>Resultados:</h2>
            <p>Suma de dígitos pares: <?php echo $sumaPares; ?></p>
            <p>Suma de dígitos impares: <?php echo $sumaImpares; ?></p>
        </div>
        
        <?php
        } else {
        ?>
        
        <form method="POST" action="">
            <label for="numero">Número:</label>
            <input type="number" name="numero" id="numero" required>
            <br><br>
            <input type="submit" name="calcular" value="Calcular">
        </form>
        
        <?php
        }
        ?>
    </div>
</body>
</html>
