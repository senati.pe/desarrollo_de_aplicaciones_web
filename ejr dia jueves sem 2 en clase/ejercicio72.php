<!DOCTYPE html>
<html>
<head>
   
<title>Cálculo del promedio de las dos notas mayores</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #7B7D7D;
        }

        h1 {
        text-align: center;
        color: #ffffff;
    }

    .container {
        width: 300px;
        margin: 0 auto;
        padding: 20px;
        border: 10px solid #ccc;
        border-radius: 5px;
        background-color: #000000;
        color: #ffffff;
    }

    label {
        display: block;
        margin-bottom: 10px;
    }

    input[type="number"] {
        width: 100%;
        padding: 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    input[type="submit"] {
        padding: 5px 10px;
        background-color: #E74C3C;
        border: none;
        color: #fff;
        cursor: pointer;
    }

    input[type="submit"]:hover {
        background-color: #616A6B;
    }

    .result {
        margin-top: 20px;
        padding: 10px;
        border: 1px solid #ccc;
        border-radius: 3px;
        background-color: #138D75;
    }
</style>
</head>
<body>
    <div class="container">
        <h1>Calculo del promedio de las dos notas mayores</h1>
    
        <?php
    if(isset($_POST['calcular'])){
        $nota1 = $_POST['nota1'];
        $nota2 = $_POST['nota2'];
        $nota3 = $_POST['nota3'];

        $notas = [$nota1, $nota2, $nota3];
        rsort($notas);

        $promedio = ($notas[0] + $notas[1]) / 2;
    ?>
    
    <div class="result">
        <h2>Resultado:</h2>
        <p>El promedio de las dos notas mayores es: <?php echo $promedio; ?></p>
    </div>
    
    <?php
    } else {
    ?>
    
    <form method="POST" action="">
        <label for="nota1">Nota 1:</label>
        <input type="number" name="nota1" id="nota1" required>
        <br><br>
        <label for="nota2">Nota 2:</label>
        <input type="number" name="nota2" id="nota2" required>
        <br><br>
        <label for="nota3">Nota 3:</label>
        <input type="number" name="nota3" id="nota3" required>
        <br><br>
        <input type="submit" name="calcular" value="Calcular">
    </form>
    
    <?php
    }
    ?>
</div>
</body>
</html>