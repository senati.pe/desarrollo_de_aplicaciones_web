<!DOCTYPE html>
<html>
<head>
    <title>Determinar si es una letra mayúscula</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            font-family: Arial, sans-serif;
        }
        
        .container {
            text-align: center;
        }
        
        h1 {
            margin-bottom: 20px;
        }
        
        label {
            display: block;
            margin-bottom: 10px;
        }
        
        input[type="text"] {
            padding: 8px;
            width: 150px;
        }
        
        input[type="button"] {
            padding: 8px 20px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }
        
        input[type="button"]:hover {
            background-color: #45a049;
        }
        
        input[type="button"].red {
            background-color: #e53935;
        }
        
        .result {
            margin-top: 20px;
            font-weight: bold;
        }
    </style>
    <script>
        function verificarLetra() {
            var letra = document.getElementById("letra").value;
            var resultado = document.getElementById("resultado");
            var boton = document.getElementById("boton");

            if (letra.match(/[A-Z]/)) {
                resultado.textContent = "Es una letra mayúscula.";
            } else {
                resultado.textContent = "No es una letra mayúscula.";
            }

            boton.classList.add("red");
        }
    </script>
</head>
<body>
    <div class="container">
        <h1>Determinar si es una letra mayúscula</h1>
        <label for="letra">Ingrese una letra:</label>
        <input type="text" id="letra" name="letra" maxlength="1" required>
        <br>
        <input type="button" value="Verificar" id="boton" onclick="verificarLetra()">
        <div class="result" id="resultado"></div>
    </div>
</body>
</html>
