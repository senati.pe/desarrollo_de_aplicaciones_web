<!DOCTYPE html>
<html>
<head>
  <title>Porcentaje de números pares, impares y neutros</title>
</head>
<body>
  <h2>Porcentaje de números pares, impares y neutros</h2>
  
  <?php
  function calcularPorcentaje($total, $parte) {
    return ($parte / $total) * 100;
  }
  

  if(isset($_POST['submit'])) {
    $numero = $_POST['numero'];
    $digitos = str_split($numero); 
    $totalDigitos = count($digitos);
    
    $contadorPares = 0;
    $contadorImpares = 0;
    $contadorNeutros = 0;
    
    foreach ($digitos as $digito) {
      if ($digito == 0) {
        $contadorNeutros++;
      } elseif ($digito % 2 == 0) {
        $contadorPares++;
      } else {
        $contadorImpares++;
      }
    }
    
    $porcentajePares = calcularPorcentaje($totalDigitos, $contadorPares);
    $porcentajeImpares = calcularPorcentaje($totalDigitos, $contadorImpares);
    $porcentajeNeutros = calcularPorcentaje($totalDigitos, $contadorNeutros);
    
    echo "<p>Número: $numero</p>";
    echo "<p>Porcentaje de números pares: $porcentajePares%</p>";
    echo "<p>Porcentaje de números impares: $porcentajeImpares%</p>";
    echo "<p>Porcentaje de números neutros (0): $porcentajeNeutros%</p>";
  }
  ?>

  <form method="post" action="">
    <label for="numero">Número:</label>
    <input type="number" id="numero" name="numero" required><br>

    <input type="submit" name="submit" value="Calcular porcentaje">
  </form>
</body>
</html>
