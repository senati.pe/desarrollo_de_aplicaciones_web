<!DOCTYPE html>
<html>
<head>
  <title>Verificar dígito en un número</title>
</head>
<body>
  <h2>Verificar dígito en un número</h2>
  
  <?php
  function verificarDigito($numero, $digito) {
    $numero = (string) $numero; 
    return strpos($numero, (string) $digito) !== false;
  }
  
  if(isset($_POST['submit'])) {
    $numero = $_POST['numero'];
    $digito = $_POST['digito'];
    
    if(verificarDigito($numero, $digito)) {
      echo "El dígito $digito está presente en el número $numero.";
    } else {
      echo "El dígito $digito no está presente en el número $numero.";
    }
  }
  ?>

  <form method="post" action="">
    <label for="numero">Número:</label>
    <input type="number" id="numero" name="numero" required><br>

    <label for="digito">Dígito a buscar:</label>
    <input type="number" id="digito" name="digito" required><br>

    <input type="submit" name="submit" value="Verificar">
  </form>
</body>
</html>
