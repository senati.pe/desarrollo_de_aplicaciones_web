<!DOCTYPE html>
<html>
<head>
    <title>Verificador de Palíndromos</title>
</head>
<body>
    <h1>Verificador de Palíndromos</h1>

    <?php
    function esPalindromo($palabra) {
        $palabra = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $palabra)); 
        return $palabra === strrev($palabra); 
    }

    if (isset($_POST['palabra'])) {
        $palabra = $_POST['palabra'];
        $esPalindromo = esPalindromo($palabra);

        if ($esPalindromo) {
            echo "<p>La palabra '$palabra' es un palíndromo.</p>";
        } else {
            echo "<p>La palabra '$palabra' no es un palíndromo.</p>";
        }
    }
    ?>

    <form method="POST">
        <label for="palabra">Ingresa una palabra:</label><br>
        <input type="text" name="palabra" id="palabra"><br>
        <input type="submit" value="Verificar">
    </form>
</body>
</html>
