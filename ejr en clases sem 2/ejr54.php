<!DOCTYPE html>
<html>
<head>
    <title>Ordenador de Números</title>
</head>
<body>
    <h1>Ordenar los Números</h1>

    <?php
    function ordenarNumeros($numeros, $forma) {
        if ($forma === 'A') {
            sort($numeros); 
        } elseif ($forma === 'D') {
            rsort($numeros); 
        }

        return $numeros;
    }

    if (isset($_POST['numeros']) && isset($_POST['forma'])) {
        $numeros = explode(',', $_POST['numeros']);
        $forma = $_POST['forma'];

        if (count($numeros) === 5) {
            $numerosOrdenados = ordenarNumeros($numeros, $forma);

            echo "<p>Números ordenados de forma '$forma': " . implode(', ', $numerosOrdenados) . "</p>";
        } else {
            echo "<p>Debes ingresar exactamente 5 números separados por comas.</p>";
        }
    }
    ?>

    <form method="POST">
        <label for="numeros">Ingresa 5 números separado por comas:</label><br>
        <input type="text" name="numeros" id="numeros"><br><br>
        <label for="forma">Selecciona la forma de orden:</label><br>
        <select name="forma" id="forma">
            <option value="A">Asccendente</option>
            <option value="D">Descendente</option>
        </select><br><br>
        <input type="submit" value="Ordenar">
    </form>
</body>
</html>
