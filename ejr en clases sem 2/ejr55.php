<!DOCTYPE html>
<html>
<head>
    <title>Contador de N repetidos</title>
</head>
<body>
    <h2>Contar numeros repetidos</h2>

    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="numbers">Ingrese 6 números separados por comas:</label><br>
        <input type="text" name="numbers" id="numbers" required><br>
        <input type="submit" value="Contar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $inputNumbers = $_POST["numbers"];
        $numbersArray = explode(",", $inputNumbers);

        if (count($numbersArray) == 6) {
            $repeatedNumbers = array_count_values($numbersArray);

            echo "<h3>el Resulatado es:</h3>";
            echo "<ul>";
            foreach ($repeatedNumbers as $number => $count) {
                if ($count > 1) {
                    echo "<li>el numero $number se repite $count veces.</li>";
                }
            }
            echo "</ul>";
        } else {
            echo "<p>Ingrese 6 numeros con Comas.</p>";
        }
    }
    ?>
</body>
</html>
