<!DOCTYPE html>
<html>
<head>
    <title>Contador de dígitos "0"</title>
</head>
<body>
    <h1>Contador de dígitos "0"</h1>

    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Ingrese un número entero: <input type="number" name="numero"><br>
        <input type="submit" value="Contar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero = $_POST["numero"];

        $contador = 0;
        $numero_absoluto = abs($numero); 

        while ($numero_absoluto > 0) {
            $digito = $numero_absoluto % 10; 
            if ($digito == 0) {
                $contador++;
            }
            $numero_absoluto = intdiv($numero_absoluto, 10); 
        }

        echo "<h2>Resultados:</h2>";
        echo "Cantidad de dígitos '0': " . $contador . "<br>";
    }
    ?>

</body>
</html>
