<!DOCTYPE html>
<html>
<head>
    <title>Suma y producto de los N primeros múltiplos de 3</title>
</head>
<body>
    <h1>Suma y producto de los N primeros múltiplos de 3</h1>

    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Ingrese un número entero N: <input type="number" name="n"><br>
        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $n = $_POST["n"];

        $suma = 0;
        $producto = 1;

        for ($i = 1; $i <= $n; $i++) {
            $multiplo = $i * 3;
            $suma += $multiplo;
            $producto *= $multiplo;
        }

        echo "<h2>Resultados:</h2>";
        echo "Suma de los $n primeros múltiplos de 3: " . $suma . "<br>";
        echo "Producto de los $n primeros múltiplos de 3: " . $producto . "<br>";
    }
    ?>

</body>
</html>
