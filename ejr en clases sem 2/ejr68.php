<!DOCTYPE html>
<html>
<head>
    <title>Contador de palabras repetidas</title>
</head>
<body>
    <h1>Contador de palabras repetidas</h1>
    <form method="POST">
        <label for="frase">Ingresa una frase:</label><br>
        <textarea name="frase" id="frase" rows="4" cols="50"></textarea><br>
        <input type="submit" value="Contar">
    </form>

    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $frase = $_POST['frase'];
        $palabras = str_word_count($frase, 1);
        $repetidas = array_count_values($palabras);

        echo "<h2>Palabras repetidas:</h2>";
        foreach ($repetidas as $palabra => $cantidad) {
            echo "<p>$palabra: $cantidad</p>";
        }
    }
    ?>
</body>
</html>
