<!DOCTYPE html>
<html>
<head>
  <title>Números primos en un rango</title>
</head>
<body>
  <h2>Números primos en un rango</h2>
  
  <?php
  function esPrimo($numero) {
    if ($numero < 2) {
      return false;
    }
  
    for ($i = 2; $i <= sqrt($numero); $i++) {
      if ($numero % $i === 0) {
        return false;
      }
    }
  
    return true;
  }
  
  if(isset($_POST['submit'])) {
    $inicio = $_POST['inicio'];
    $fin = $_POST['fin'];
    $contadorPrimos = 0;
    
    for ($numero = $inicio; $numero <= $fin; $numero++) {
      if (esPrimo($numero)) {
        $contadorPrimos++;
      }
    }
    
    echo "Hay $contadorPrimos números primos en el rango [$inicio, $fin].";
  }
  ?>

  <form method="post" action="">
    <label for="inicio">Inicio del rango:</label>
    <input type="number" id="inicio" name="inicio" required><br>

    <label for="fin">Fin del rango:</label>
    <input type="number" id="fin" name="fin" required><br>

    <input type="submit" name="submit" value="Contar primos">
  </form>
</body>
</html>
