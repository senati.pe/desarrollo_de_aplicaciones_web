<!DOCTYPE html>
<html>
<head>
    <title>Contador de números pares e impares</title>
</head>
<body>
    <h1>Contador de números pares e impares</h1>

    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        Inicio del rango: <input type="number" name="inicio"><br>
        Fin del rango: <input type="number" name="fin"><br>
        <input type="submit" value="Contar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $inicio = $_POST["inicio"];
        $fin = $_POST["fin"];

        $pares = 0;
        $impares = 0;

        for ($i = $inicio; $i <= $fin; $i++) {
            if ($i % 5 == 0) {
                continue; 
            }

            if ($i % 2 == 0) {
                $pares++;
            } else {
                $impares++;
            }
        }

        echo "<h2>Resultados:</h2>";
        echo "Cantidad de números pares: " . $pares . "<br>";
        echo "Cantidad de números impares: " . $impares . "<br>";
    }
    ?>

</body>
</html>
