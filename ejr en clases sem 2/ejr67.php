<!DOCTYPE html>
<html>
<head>
    <title>PALABRAS PALINDROMOS</title>
</head>
<body>
    <h1>PALABRA PALINDROMO</h1>

    <?php
    function esPalindromo($palabra) {
        $palabra = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $palabra)); 
        return $palabra === strrev($palabra); 
    }

    if (isset($_POST['frase'])) {
        $frase = $_POST['frase'];
        $palabras = explode(' ', $frase); 
        $contador = 0;

        foreach ($palabras as $palabra) {
            if (esPalindromo($palabra)) {
                $contador++;
            }
        }

        echo "<p>La frase ingresada tiene $contador Palabra  Palindromo.</p>";
    }
    ?>

    <form method="POST">
        <label for="frase">Ingresa una frase Palindromo:</label><br>
        <textarea name="frase" id="frase" rows="4" cols="50"></textarea><br>
        <input type="submit" value="Contar">
    </form>
</body>
</html>
