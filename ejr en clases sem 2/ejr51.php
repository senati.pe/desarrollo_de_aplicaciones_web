<!DOCTYPE html>
<html>
<head>
    <title>Calcula la Suma y el Promedio</title>
</head>
<body>
    <h1>Calcula la Suma y el Promedio</h1>

    <?php
    if (isset($_POST['numeros'])) {
        $numeros = explode(',', $_POST['numeros']);

        if (count($numeros) === 4) {
            $numeros = array_map('intval', $numeros);

            $suma = array_sum($numeros);

            $promedio = $suma / count($numeros);

            echo "<p>La suma de los números es: $suma</p>";
            echo "<p>El promedio de los números es: $promedio</p>";
        } else {
            echo "<p>Debes ingresar exactamente 4 números separados por comas.</p>";
        }
    }
    ?>

    <form method="POST">
        <label for="numeros">Ingresa 4 números separados por comas:</label><br>
        <input type="text" name="numeros" id="numeros"><br><br>
        <input type="submit" value="Calcular">
    </form>
</body>
</html>
