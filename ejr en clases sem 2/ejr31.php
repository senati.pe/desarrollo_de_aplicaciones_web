<!DOCTYPE html>
<html>
<head>
	<title>Factorial de un número</title>
</head>
<body>
	<form method="post">
		<label>Ingrese un número entero: </label>
		<input type="number" name="numero">
		<button type="submit">Calcular factorial</button>
	</form>

	<?php
		if(isset($_POST['numero'])){
			$numero = $_POST['numero'];
			$factorial = 1;
			for($i=1; $i<=$numero; $i++){
				$factorial *= $i;
			}
			echo "<p>El factorial de $numero es: $factorial</p>";
		}
	?>
</body>
</html>
