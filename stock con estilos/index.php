<?php require_once 'header_idx.php'; ?>
<style>
  /* Estilo para la tabla */
  .table {
    width: 80%;
    border-collapse: collapse;
  }

  .table td {
    padding: 0px;
    text-align: center;
    background-color: #FFFFFF;
  }

  /* Estilo para los enlaces */
  .table a {
    text-decoration: none;
    color: #0066CC;
    font-weight: bold;
  }

  .table a:hover {
    text-decoration: underline;
  }
</style>

<table class="table">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><a href="productos" style="color: #FF0000;">productos</a></td>
    <td><a href="compras" style="color: #00FF00;">Compras</a></td>
    <td><a href="ventas" style="color: #0000FF;">ventas</a></td>
    <td><a href="header_idx.php" style="color: #FF00FF;">stock</a></td>
    <link rel="stylesheet" href="estilos.css">

  </tr>
</table>
</div>
</div>

<?php require_once 'footer.php'; ?>
