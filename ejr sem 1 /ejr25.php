<!DOCTYPE html>
<html>
<head>
    <title>Monto de descuento al sueldo</title>
</head>
<body>
    <h1>Monto de descuento al sueldo</h1>

    <form method="post" action="">
        <label for="sexo">Sexo:</label>
        <select name="sexo" id="sexo" required>
            <option value="hombre">Hombre</option>
            <option value="mujer">Mujer</option>
        </select>
        <br>
        <label for="categoria">Categoría:</label>
        <select name="categoria" id="categoria" required>
            <option value="obrero">Obrero</option>
            <option value="empleado">Empleado</option>
        </select>
        <br>
        <label for="sueldo">Sueldo:</label>
        <input type="number" name="sueldo" id="sueldo" step="0.01" required>
        <br>
        <input type="submit" value="Calcular descuento">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sexo = $_POST["sexo"];
        $categoria = $_POST["categoria"];
        $sueldo = $_POST["sueldo"];
        $descuento = 0;

        if ($sexo == "hombre" && $categoria == "obrero") {
            $descuento = $sueldo * 0.15;
        } elseif ($sexo == "hombre" && $categoria == "empleado") {
            $descuento = $sueldo * 0.20;
        } elseif ($sexo == "mujer" && $categoria == "obrero") {
            $descuento = $sueldo * 0.10;
        } elseif ($sexo == "mujer" && $categoria == "empleado") {
            $descuento = $sueldo * 0.15;
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>El monto del descuento al sueldo es: $descuento</p>";
    }
    ?>
</body>
</html>
