<!DOCTYPE html>
<html>
<head>
    <title>Números enteros incluidos</title>
</head>
<body>
    <?php
    if (isset($_POST['submit'])) {
        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];

        $count = abs($num2 - $num1) + 1;
        
        echo "<h1>Resultado</h1>";
        echo "El rango entre $num1 y $num2 contiene $count números enteros.";
    }
    ?>
    
    <h1>Números enteros incluidos</h1>
    <form method="POST">
        <label for="num1">Número 1:</label>
        <input type="number" id="num1" name="num1" required>
        <br>
        <label for="num2">Número 2:</label>
        <input type="number" id="num2" name="num2" required>
        <br>
        <input type="submit" name="submit" value="Calcular">
    </form>
</body>
</html>
