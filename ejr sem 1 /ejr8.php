<!DOCTYPE html>
<html>
<head>
    <title>Cálculo de área y perímetro de un rectángulo</title>
</head>
<body>
    <h1>Cálculo de área y perímetro de un rectángulo</h1>

    <form method="post" action="">
        <label for="length">Longitud:</label>
        <input type="number" name="length" id="length" required>
        <br>
        <label for="width">Ancho:</label>
        <input type="number" name="width" id="width" required>
        <br>
        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $length = $_POST["length"];
        $width = $_POST["width"];

        $area = $length * $width;
        $perimeter = 2 * ($length + $width);

        echo "<h2>Resultado:</h2>";
        echo "<p>El área del rectángulo es: $area unidades cuadradas.</p>";
        echo "<p>El perímetro del rectángulo es: $perimeter unidades.</p>";
    }
    ?>
</body>
</html>
