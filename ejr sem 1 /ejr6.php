<!DOCTYPE html>
<html>
<head>
    <title>Conversión de horas a minutos y segundos</title>
</head>
<body>
    <h1>Conversión de horas a minutos y segundos</h1>

    <form method="post" action="">
        <label for="hours">Horas:</label>
        <input type="number" name="hours" id="hours" required>
        <input type="submit" value="Convertir">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $hours = $_POST["hours"];

        $minutes = $hours * 60;
        $seconds = $hours * 3600;

        echo "<h2>Resultado:</h2>";
        echo "<p>$hours horas equivale a $minutes minutos y $seconds segundos.</p>";
    }
    ?>
</body>
</html>
