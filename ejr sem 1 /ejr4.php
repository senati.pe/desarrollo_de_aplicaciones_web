<!DOCTYPE html>
<html>
<head>
    <title>Porcentaje de números enteros</title>
</head>
<body>
    <?php
    if (isset($_POST['submit'])) {
        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];
        $num3 = $_POST['num3'];
        $num4 = $_POST['num4'];

        $suma = $num1 + $num2 + $num3 + $num4;

        $porcentaje1 = ($num1 / $suma) * 100;
        $porcentaje2 = ($num2 / $suma) * 100;
        $porcentaje3 = ($num3 / $suma) * 100;
        $porcentaje4 = ($num4 / $suma) * 100;
        
        echo "<h1>Porcentaje de números enteros</h1>";
        echo "Suma de los números ingresados: $suma<br>";
        echo "Porcentaje del número $num1: $porcentaje1%<br>";
        echo "Porcentaje del número $num2: $porcentaje2%<br>";
        echo "Porcentaje del número $num3: $porcentaje3%<br>";
        echo "Porcentaje del número $num4: $porcentaje4%";
    }
    ?>
    
    <h1>Porcentaje de números enteros</h1>
    <form method="POST">
        <label for="num1">Número 1:</label>
        <input type="number" id="num1" name="num1" required>
        <br>
        <label for="num2">Número 2:</label>
        <input type="number" id="num2" name="num2" required>
        <br>
        <label for="num3">Número 3:</label>
        <input type="number" id="num3" name="num3" required>
        <br>
        <label for="num4">Número 4:</label>
        <input type="number" id="num4" name="num4" required>
        <br>
        <input type="submit" name="submit" value="Calcular">
    </form>
</body>
</html>
