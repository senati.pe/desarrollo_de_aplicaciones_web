<!DOCTYPE html>
<html>
<head>
    <title>Ordenar números enteros</title>
</head>
<body>
    <h1>Ordenar números enteros</h1>

    <form method="post" action="">
        <label for="num1">Número 1:</label>
        <input type="number" name="num1" id="num1" required>
        <br>
        <label for="num2">Número 2:</label>
        <input type="number" name="num2" id="num2" required>
        <br>
        <label for="num3">Número 3:</label>
        <input type="number" name="num3" id="num3" required>
        <br>
        <input type="submit" value="Ordenar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];

        $ascending = array($num1, $num2, $num3);
        sort($ascending);

        $descending = array($num1, $num2, $num3);
        rsort($descending);

        echo "<h2>Resultado:</h2>";
        echo "<p>Orden ascendente: " . implode(", ", $ascending) . "</p>";
        echo "<p>Orden descendente: " . implode(", ", $descending) . "</p>";
    }
    ?>
</body>
</html>
