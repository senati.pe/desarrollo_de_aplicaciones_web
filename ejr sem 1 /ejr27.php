<!DOCTYPE html>
<html>
<head>
    <title>Obtener estado civil</title>
</head>
<body>
    <h1>Obtener estado civil</h1>

    <form method="post" action="">
        <label for="codigo">Código:</label>
        <input type="number" name="codigo" id="codigo" required>
        <br>
        <input type="submit" value="Obtener estado civil">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $codigo = $_POST["codigo"];
        $estado_civil = "";

        switch ($codigo) {
            case 0:
                $estado_civil = "Soltero";
                break;
            case 1:
                $estado_civil = "Casado";
                break;
            case 2:
                $estado_civil = "Divorciado";
                break;
            case 3:
                $estado_civil = "Viudo";
                break;
            default:
                $estado_civil = "Código inválido";
                break;
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>El estado civil correspondiente al código $codigo es: $estado_civil</p>";
    }
    ?>
</body>
</html>
