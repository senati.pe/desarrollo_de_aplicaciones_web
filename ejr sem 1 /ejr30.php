<!DOCTYPE html>
<html>
<head>
    <title>Días faltantes para el fin de año</title>
</head>
<body>
    <h1>Días faltantes para el fin de año</h1>

    <form method="post" action="">
        <label for="fecha">Fecha:</label>
        <input type="date" name="fecha" id="fecha" required>
        <br>
        <input type="submit" value="Calcular días faltantes">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $fecha = $_POST["fecha"];
        $hoy = new DateTime();
        $fin_de_ano = new DateTime(date("Y") . "-12-31");
        $dias_faltantes = $fin_de_ano->diff($hoy)->days;

        echo "<h2>Resultado:</h2>";
        echo "<p>Quedan $dias_faltantes días para el fin de año.</p>";
    }
    ?>
</body>
</html>
