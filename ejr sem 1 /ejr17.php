<!DOCTYPE html>
<html>
<head>
    <title>Cálculo de saldo actual</title>
</head>
<body>
    <h1>Cálculo de saldo actual</h1>

    <form method="post" action="">
        <label for="saldo_anterior">Saldo anterior:</label>
        <input type="number" name="saldo_anterior" id="saldo_anterior" required>
        <br>
        <label for="tipo_movimiento">Tipo de movimiento:</label>
        <select name="tipo_movimiento" id="tipo_movimiento" required>
            <option value="R">Retiro</option>
            <option value="D">Depósito</option>
        </select>
        <br>
        <label for="monto">Monto de la transacción:</label>
        <input type="number" name="monto" id="monto" required>
        <br>
        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $saldo_anterior = $_POST["saldo_anterior"];
        $tipo_movimiento = $_POST["tipo_movimiento"];
        $monto = $_POST["monto"];

        if ($tipo_movimiento == "R") {
            $saldo_actual = $saldo_anterior - $monto;
        } elseif ($tipo_movimiento == "D") {
            $saldo_actual = $saldo_anterior + $monto;
        } else {
            $saldo_actual = $saldo_anterior;
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>Saldo actual: $saldo_actual</p>";
    }
    ?>
</body>
</html>
