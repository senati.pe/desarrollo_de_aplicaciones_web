<!DOCTYPE html>
<html>
<head>
    <title>Determinar si una persona es mayor o menor de edad</title>
</head>
<body>
    <h1>Determinar si una persona es mayor o menor de edad</h1>

    <form method="post" action="">
        <label for="age">Edad:</label>
        <input type="number" name="age" id="age" required>
        <input type="submit" value="Verificar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $age = $_POST["age"];

        if ($age >= 18) {
            echo "<h2>Resultado:</h2>";
            echo "<p>La persona es mayor de edad.</p>";
        } else {
            echo "<h2>Resultado:</h2>";
            echo "<p>La persona es menor de edad.</p>";
        }
    }
    ?>
</body>
</html>
