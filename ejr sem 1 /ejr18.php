<!DOCTYPE html>
<html>
<head>
    <title>Determinar el número mayor</title>
</head>
<body>
    <h1>Determinar el número mayor</h1>

    <form method="post" action="">
        <label for="numero1">Número 1:</label>
        <input type="number" name="numero1" id="numero1" required>
        <br>
        <label for="numero2">Número 2:</label>
        <input type="number" name="numero2" id="numero2" required>
        <br>
        <input type="submit" value="Comparar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero1 = $_POST["numero1"];
        $numero2 = $_POST["numero2"];

        if ($numero1 > $numero2) {
            $resultado = "El número $numero1 es mayor que $numero2";
        } elseif ($numero1 < $numero2) {
            $resultado = "El número $numero2 es mayor que $numero1";
        } else {
            $resultado = "El número $numero1 es igual a $numero2";
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>$resultado</p>";
    }
    ?>
</body>
</html>
