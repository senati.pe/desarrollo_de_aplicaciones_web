<!DOCTYPE html>
<html>
<head>
    <title>Expresión de una cantidad en metros, decímetros, centímetros y milímetros</title>
</head>
<body>
    <?php
    if (isset($_POST['submit'])) {
        $milimetros = $_POST['milimetros'];

        $metros = floor($milimetros / 1000);
        $resto_metros = $milimetros % 1000;

        $decimetros = floor($resto_metros / 100);
        $resto_decimetros = $resto_metros % 100;

        $centimetros = floor($resto_decimetros / 10);
        $milimetros = $resto_decimetros % 10;

        echo "<h1>Resultado</h1>";
        echo "$milimetros milímetros equivalen a: <br>";
        echo "Metros: $metros <br>";
        echo "Decímetros: $decimetros <br>";
        echo "Centímetros: $centimetros <br>";
        echo "Milímetros: $milimetros";
    }
    ?>
    
    <h1>Expresión de una cantidad en metros, decímetros, centímetros y milímetros</h1>
    <form method="POST">
        <label for="milimetros">Cantidad en milímetros:</label>
        <input type="number" id="milimetros" name="milimetros" required>
        <br>
        <input type="submit" name="submit" value="Calcular">
    </form>
</body>
</html>
