<!DOCTYPE html>
<html>
<head>
    <title>Suma y Resta de números enteros</title>
</head>
<body>
    <?php
    if (isset($_POST['submit'])) {
        $num1 = $_POST['num1'];
        $num2 = $_POST['num2'];
        
        $suma = $num1 + $num2;
        $resta = $num1 - $num2;
        
        echo "<h1>Resultado</h1>";
        echo "La suma de $num1 y $num2 es: $suma<br>";
        echo "La resta de $num1 y $num2 es: $resta";
    }
    ?>
    
    <h1>Suma y Resta de números enteros</h1>
    <form method="POST">
        <label for="num1">Número 1:</label>
        <input type="number" id="num1" name="num1" required>
        <br>
        <label for="num2">Número 2:</label>
        <input type="number" id="num2" name="num2" required>
        <br>
        <input type="submit" name="submit" value="Calcular">
    </form>
</body>
</html>
