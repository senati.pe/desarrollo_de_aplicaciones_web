<!DOCTYPE html>
<html>
<head>
    <title>Área y Perímetro de un Cuadrado</title>
</head>
<body>
    <?php
    if (isset($_POST['submit'])) {
        $lado = $_POST['lado'];

        $area = $lado * $lado;
        $perimetro = 4 * $lado;
        
        echo "<h1>Resultado</h1>";
        echo "Lado del cuadrado: $lado <br>";
        echo "Área del cuadrado: $area <br>";
        echo "Perímetro del cuadrado: $perimetro";
    }
    ?>
    
    <h1>Área y Perímetro de un Cuadrado</h1>
    <form method="POST">
        <label for="lado">Lado del Cuadrado:</label>
        <input type="number" id="lado" name="lado" required>
        <br>
        <input type="submit" name="submit" value="Calcular">
    </form>
</body>
</html>
