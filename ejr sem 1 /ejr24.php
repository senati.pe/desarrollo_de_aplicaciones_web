
<!DOCTYPE html>
<html>
<head>
    <title>Nombre del canal de televisión</title>
</head>
<body>
    <h1>Nombre del canal de televisión</h1>

    <form method="post" action="">
        <label for="numero_canal">Número del canal:</label>
        <input type="number" name="numero_canal" id="numero_canal" min="1" max="99" required>
        <br>
        <input type="submit" value="Obtener">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero_canal = $_POST["numero_canal"];
        $canales = array(
            1 => "Canal 1",
            2 => "Canal 2",
            3 => "Canal 3",
        );

        if (isset($canales[$numero_canal])) {
            $nombre_canal = $canales[$numero_canal];
            echo "<h2>Resultado:</h2>";
            echo "<p>El canal número $numero_canal es $nombre_canal</p>";
        } else {
            echo "<h2>Error:</h2>";
            echo "<p>No se encontró el canal correspondiente al número ingresado.</p>";
        }
    }
    ?>
</body>
</html>
