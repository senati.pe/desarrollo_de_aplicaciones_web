<!DOCTYPE html>
<html>
<head>
    <title>Conversión de grados sexagesimales a centesimales</title>
</head>
<body>
    <h1>Conversión de grados sexagesimales a centesimales</h1>

    <form method="post" action="">
        <label for="degrees">Grados:</label>
        <input type="number" name="degrees" id="degrees" step="0.01" required>
        <input type="submit" value="Convertir">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $degrees = $_POST["degrees"];

        $centesimal = $degrees * 100 / 90;

        echo "<h2>Resultado:</h2>";
        echo "<p>$degrees grados sexagesimales equivale a $centesimal grados centesimales.</p>";
    }
    ?>
</body>
</html>
