<!DOCTYPE html>
<html>
<head>
    <title>Nombre del operador aritmético</title>
</head>
<body>
    <h1>Nombre del operador aritmético</h1>

    <form method="post" action="">
        <label for="operador">Operador aritmético:</label>
        <input type="text" name="operador" id="operador" required>
        <br>
        <input type="submit" value="Obtener">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $operador = $_POST["operador"];

        switch ($operador) {
            case "+":
                $nombre_operador = "suma";
                break;
            case "-":
                $nombre_operador = "resta";
                break;
            case "*":
                $nombre_operador = "multiplicación";
                break;
            case "/":
                $nombre_operador = "división";
                break;
            default:
                $nombre_operador = "desconocido";
                break;
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>El operador $operador corresponde a $nombre_operador</p>";
    }
    ?>
</body>
</html>
