<!DOCTYPE html>
<html>
<head>
    <title>Obtener mes en letras</title>
</head>
<body>
    <h1>Obtener mes en letras</h1>

    <form method="post" action="">
        <label for="numero_mes">Número de mes:</label>
        <input type="number" name="numero_mes" id="numero_mes" min="1" max="12" required>
        <br>
        <input type="submit" value="Obtener">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero_mes = $_POST["numero_mes"];
        $meses = array(
            1 => "enero",
            2 => "febrero",
            3 => "marzo",
            4 => "abril",
            5 => "mayo",
            6 => "junio",
            7 => "julio",
            8 => "agosto",
            9 => "septiembre",
            10 => "octubre",
            11 => "noviembre",
            12 => "diciembre"
        );

        if (isset($meses[$numero_mes])) {
            $mes_en_letras = $meses[$numero_mes];
            echo "<h2>Resultado:</h2>";
            echo "<p>El número de mes $numero_mes corresponde a $mes_en_letras</p>";
        } else {
            echo "<h2>Error:</h2>";
            echo "<p>El número de mes ingresado no es válido.</p>";
        }
    }
    ?>
</body>
</html>
