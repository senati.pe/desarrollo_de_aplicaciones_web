<!DOCTYPE html>
<html>
<head>
    <title>Determinar el doble, triple o cero de un número</title>
</head>
<body>
    <h1>Determinar el doble, triple o cero de un número</h1>

    <form method="post" action="">
        <label for="number">Número:</label>
        <input type="number" name="number" id="number" required>
        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $number = $_POST["number"];

        if ($number > 0) {
            $result = $number * 2;
            $message = "El doble del número es: $result";
        } elseif ($number < 0) {
            $result = $number * 3;
            $message = "El triple del número es: $result";
        } else {
            $message = "El número es neutro, el resultado es: 0";
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>$message</p>";
    }
    ?>
</body>
</html>
