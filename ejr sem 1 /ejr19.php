<!DOCTYPE html>
<html>
<head>
    <title>Determinar si forman un triángulo</title>
</head>
<body>
    <h1>Determinar si forman un triángulo</h1>

    <form method="post" action="">
        <label for="longitud1">Longitud 1:</label>
        <input type="number" name="longitud1" id="longitud1" required>
        <br>
        <label for="longitud2">Longitud 2:</label>
        <input type="number" name="longitud2" id="longitud2" required>
        <br>
        <label for="longitud3">Longitud 3:</label>
        <input type="number" name="longitud3" id="longitud3" required>
        <br>
        <input type="submit" value="Verificar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $longitud1 = $_POST["longitud1"];
        $longitud2 = $_POST["longitud2"];
        $longitud3 = $_POST["longitud3"];

        if ($longitud1 < ($longitud2 + $longitud3) && $longitud2 < ($longitud1 + $longitud3) && $longitud3 < ($longitud1 + $longitud2)) {
            $resultado = "Las longitudes forman un triángulo.";
        } else {
            $resultado = "Las longitudes no forman un triángulo.";
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>$resultado</p>";
    }
    ?>
</body>
</html>
