<!DOCTYPE html>
<html>
<head>
    <title>Obtener ciudad a visitar</title>
</head>
<body>
    <h1>Obtener ciudad a visitar</h1>

    <form method="post" action="">
        <label for="sexo">Sexo:</label>
        <select name="sexo" id="sexo" required>
            <option value="masculino">Masculino</option>
            <option value="femenino">Femenino</option>
        </select>
        <br>
        <label for="puntaje">Puntaje en el examen:</label>
        <input type="number" name="puntaje" id="puntaje" required>
        <br>
        <input type="submit" value="Obtener ciudad">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sexo = $_POST["sexo"];
        $puntaje = $_POST["puntaje"];
        $ciudad = "";

        if ($sexo == "masculino") {
            if ($puntaje >= 18 && $puntaje <= 35) {
                $ciudad = "Arequipa";
            } elseif ($puntaje >= 36 && $puntaje <= 75) {
                $ciudad = "Cuzco";
            } elseif ($puntaje > 75) {
                $ciudad = "Iquitos";
            }
        } elseif ($sexo == "femenino") {
            if ($puntaje >= 18 && $puntaje <= 35) {
                $ciudad = "Cuzco";
            } elseif ($puntaje >= 36 && $puntaje <= 75) {
                $ciudad = "Iquitos";
            } elseif ($puntaje > 75) {
                $ciudad = "Arequipa";
            }
        }

        echo "<h2>Resultado:</h2>";
        echo "<p>La ciudad que visitará es: $ciudad</p>";
    }
    ?>
</body>
</html>
