<!DOCTYPE html>
<html>
<head>
    <title>Determinar si dos números son iguales o diferentes</title>
</head>
<body>
    <h1>Determinar si dos números son iguales o diferentes</h1>

    <form method="post" action="">
        <label for="num1">Número 1:</label>
        <input type="number" name="num1" id="num1" required>
        <br>
        <label for="num2">Número 2:</label>
        <input type="number" name="num2" id="num2" required>
        <br>
        <input type="submit" value="Determinar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];

        if ($num1 == $num2) {
            echo "<h2>Resultado:</h2>";
            echo "<p>Los números son iguales.</p>";
        } else {
            echo "<h2>Resultado:</h2>";
            echo "<p>Los números son diferentes.</p>";
        }
    }
    ?>
</body>
</html>
