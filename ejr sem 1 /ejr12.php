<!DOCTYPE html>
<html>
<head>
    <title>Determinar el número menor entre dos números enteros</title>
</head>
<body>
    <h1>Determinar el número menor entre dos números enteros</h1>

    <form method="post" action="">
        <label for="num1">Número 1:</label>
        <input type="number" name="num1" id="num1" required>
        <br>
        <label for="num2">Número 2:</label>
        <input type="number" name="num2" id="num2" required>
        <br>
        <input type="submit" value="Determinar">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];

        $min = min($num1, $num2);

        echo "<h2>Resultado:</h2>";
        echo "<p>El número menor es: $min</p>";
    }
    ?>
</body>
</html>
