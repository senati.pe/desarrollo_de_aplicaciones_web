<!DOCTYPE html>
<html>
<head>
    <title>Día de la semana</title>
</head>
<body>
    <h1>Día de la semana</h1>

    <form method="post" action="">
        <label for="numero">Número del 1 al 7:</label>
        <input type="number" name="numero" id="numero" min="1" max="7" required>
        <br>
        <input type="submit" value="Obtener">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $numero = $_POST["numero"];
        $dias_semana = array(
            1 => "domingo",
            2 => "lunes",
            3 => "martes",
            4 => "miércoles",
            5 => "jueves",
            6 => "viernes",
            7 => "sábado"
        );

        if (isset($dias_semana[$numero])) {
            $dia_semana = $dias_semana[$numero];
            echo "<h2>Resultado:</h2>";
            echo "<p>El número $numero corresponde al día $dia_semana</p>";
        } else {
            echo "<h2>Error:</h2>";
            echo "<p>El número ingresado no es válido.</p>";
        }
    }
    ?>
</body>
</html>
