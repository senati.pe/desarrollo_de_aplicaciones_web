<!DOCTYPE html>
<html>
<head>
    <title>Conversión de grados Fahrenheit a Celsius y Kelvin</title>
</head>
<body>
    <h1>Conversión de grados Fahrenheit a Celsius y Kelvin</h1>

    <form method="post" action="">
        <label for="fahrenheit">Grados Fahrenheit:</label>
        <input type="number" name="fahrenheit" id="fahrenheit" required>
        <input type="submit" value="Convertir">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $fahrenheit = $_POST["fahrenheit"];

        $celsius = ($fahrenheit - 32) * 5 / 9;
        $kelvin = ($fahrenheit + 459.67) * 5 / 9;

        echo "<h2>Resultado:</h2>";
        echo "<p>$fahrenheit grados Fahrenheit equivale a $celsius grados Celsius y $kelvin grados Kelvin.</p>";
    }
    ?>
</body>
</html>
