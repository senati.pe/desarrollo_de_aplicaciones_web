<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de pago por manzanas</title>
</head>
<body>
    <h1>Calculadora de pago por manzanas</h1>

    <form method="post" action="">
        <label for="sexo">Sexo:</label>
        <select name="sexo" id="sexo" required>
            <option value="hombre">Hombre</option>
            <option value="mujer">Mujer</option>
        </select>
        <br>
        <label for="categoria">Categoría:</label>
        <select name="categoria" id="categoria" required>
            <option value="obrero">Obrero</option>
            <option value="empleado">Empleado</option>
        </select>
        <br>
        <label for="kilos">Cantidad de kilos de manzanas:</label>
        <input type="number" name="kilos" id="kilos" step="0.01" required>
        <br>
        <input type="submit" value="Calcular pago">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sexo = $_POST["sexo"];
        $categoria = $_POST["categoria"];
        $kilos = $_POST["kilos"];
        $monto_pagar = 0;

        if ($sexo == "hombre" && $categoria == "obrero") {
            if ($kilos <= 2) {
                $monto_pagar = $kilos * 1; // Precio por kilo sin descuento
            } elseif ($kilos <= 5) {
                $monto_pagar = $kilos * 0.9; // 10% de descuento
            } elseif ($kilos <= 10) {
                $monto_pagar = $kilos * 0.8; // 20% de descuento
            } else {
                $monto_pagar = $kilos * 0.7; // 30% de descuento
            }
        } elseif ($sexo == "hombre" && $categoria == "empleado") {
            if ($kilos <= 2) {
                $monto_pagar = $kilos * 0.85; // 15% de descuento
            } elseif ($kilos <= 5) {
                $monto_pagar = $kilos * 0.8; // 20% de descuento
            } elseif ($kilos <= 10) {
                $monto_pagar = $kilos * 0.7; // 30% de descuento
            } else {
                $monto_pagar = $kilos * 0.6; // 40% de descuento
            }
        } elseif ($sexo == "mujer" && $categoria == "obrero") {
            if ($kilos <= 2) {
                $monto_pagar = $kilos * 0.9; // 10% de descuento
            } elseif ($kilos <= 5) {
                $monto_pagar = $kilos * 0.85; // 15% de descuento
            } elseif ($kilos <= 10) {
                $monto_pagar = $kilos * 0.8; // 20% de descuento
            } else {
            $monto_pagar = $kilos * 0.7; // 30% de descuento
            }
            } elseif ($sexo == "mujer" && $categoria == "empleado") {
            if ($kilos <= 2) {
            $monto_pagar = $kilos * 0.85; // 15% de descuento
            } elseif ($kilos <= 5) {
            $monto_pagar = $kilos * 0.85; // 15% de descuento
            } elseif ($kilos <= 10) {
            $monto_pagar = $kilos * 0.85; // 15% de descuento
            } else {
            $monto_pagar = $kilos * 0.7; // 30% de descuento
            }
            }
            echo "<h2>Resultado:</h2>";
            echo "<p>El monto a pagar por $kilos kilos de manzanas es: $monto_pagar</p>";
        }
        ?>
</body>
</html>        