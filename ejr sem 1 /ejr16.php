<!DOCTYPE html>
<html>
<head>
    <title>Cálculo de promedio y estado del alumno</title>
</head>
<body>
    <h1>Cálculo de promedio y estado del alumno</h1>

    <form method="post" action="">
        <label for="nota1">Nota 1:</label>
        <input type="number" name="nota1" id="nota1" required>
        <br>
        <label for="nota2">Nota 2:</label>
        <input type="number" name="nota2" id="nota2" required>
        <br>
        <label for="nota3">Nota 3:</label>
        <input type="number" name="nota3" id="nota3" required>
        <br>
        <label for="nota4">Nota 4:</label>
        <input type="number" name="nota4" id="nota4" required>
        <br>
        <input type="submit" value="Calcular">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nota1 = $_POST["nota1"];
        $nota2 = $_POST["nota2"];
        $nota3 = $_POST["nota3"];
        $nota4 = $_POST["nota4"];

        $notas = array($nota1, $nota2, $nota3, $nota4);
        rsort($notas);

        $mejores_notas = array_slice($notas, 0, 3);
        $promedio = array_sum($mejores_notas) / 3;

        echo "<h2>Resultado:</h2>";
        echo "<p>Promedio de las tres mejores notas: $promedio</p>";

        if ($promedio >= 11) {
            echo "<p>Estado: Aprobado</p>";
        } else {
            echo "<p>Estado: Desaprobado</p>";
        }
    }
    ?>
</body>
</html>
